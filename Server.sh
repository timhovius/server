#!/bin/sh

sudo a2enmod rewrite
sudo apt-get -y install php5-curl
sudo service apache2 restart

# install node with less
sudo curl -sL https://deb.nodesource.com/setup | sudo bash -
sudo apt-get install nodejs

sudo npm -g install less
sudo npm install -g uglifycss
sudo npm install -g uglify-js